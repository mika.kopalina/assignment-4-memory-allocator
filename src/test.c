#define _GNU_SOURCE
#include <assert.h>
#include <stdio.h>
#include <sys/mman.h>

#include "test.h"
#include "util.h"

#define HEAP_SIZE 4096

static void end_test(void *heap, int query, const char *const testName) {
    munmap(heap, size_from_capacity((block_capacity) {.bytes = query}).bytes);
    fprintf(stdout, "%s%s", testName, " успешно!\n");
}

static void check_heap(const void *const heap, const char *const msg) {
    printf("%s: %s", msg, "\n");
    debug_heap(stdout, heap);
}

static struct block_header* block_get_header(void* contents){
    return (struct block_header*) (((uint8_t*)contents) - offsetof(struct block_header, contents));
}

static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

void test1(void) {
    void* heap = heap_init(HEAP_SIZE);
    assert(heap);
    if (!heap) {
        error_msg("Тест 1 провален", ", куча не инициализирована.");
        return;
    }
    check_heap(heap, "Куча после инициализации");

    void* allocator = _malloc(132);
    assert(allocator);
    if (!allocator) {
        error_msg("Тест 1 провален", ", память не выделена.");
        return;
    }
    check_heap(heap, "Куча после выделения памяти");
    heap_term();
    end_test(heap, HEAP_SIZE, "Тест 1");


}

void test2(void) {
    void *heap = heap_init(HEAP_SIZE);
    assert(heap);
    if (!heap) {
        error_msg("Тест 2 провален", ", куча не инициализирована.");
        return;
    }
    check_heap(heap, "Куча после инициализации");

    void* allocator1 = _malloc(132);
    assert(allocator1);
    void* allocator2 = _malloc(128);
    assert(allocator2);
    
    if (!allocator1 || !allocator2){
        error_msg("Тест 2 провален", ", память не выделена.");
        return;
    }
    check_heap(heap, "Куча после выделения памяти");

    _free(allocator1);
    assert(block_get_header(allocator1)->is_free);
    assert(block_get_header(allocator2)->is_free);

    if (!allocator2) {
        error_msg("Тест 2 провален", ", первый повредил второй.");
        return;
    }
    check_heap(heap, "Куча после освобождения памяти");

    assert(block_get_header(allocator1)->is_free);
    assert(block_get_header(allocator2)->is_free);
    _free(allocator2);
    heap_term();
    end_test(heap, HEAP_SIZE, "Тест 2");
}

void test3(void) {
    void *heap = heap_init(HEAP_SIZE);
    assert(heap);
    if (!heap) {
        error_msg("Тест 3 провален", ", куча не инициализирована.");
        return;
    }
    check_heap(heap, "Куча после инициализации");

    void* allocator1 = _malloc(132);
    assert(allocator1);
    void* allocator2 = _malloc(128);
    assert(allocator2);
    void* allocator3 = _malloc(190);
    assert(allocator3);
    if (!allocator1 || !allocator2 || !allocator3) {
        error_msg("Тест 3 провален", ", память не выделена.");
        return;
    }
    check_heap(heap, "Куча после выделения памяти");
    _free(allocator2);
    _free(allocator1);
    if (!allocator3){
        error_msg("Тест 3 провален", ", первый повредил второй.");
        return;
    }
    check_heap(heap, "Куча после освобождения памяти");

    assert(block_get_header(allocator1)->is_free);
    assert(block_get_header(allocator2)->is_free);
    assert(block_get_header(allocator1)->capacity.bytes==132 +
    size_from_capacity((block_capacity) {.bytes=128}).bytes);
    assert(!block_get_header(allocator3)->is_free);
    heap_term();
    end_test(heap, HEAP_SIZE, "Тест 3");
}

void test4(void) {
    void *heap = heap_init(1);
    assert(heap);
    if (!heap) {
        error_msg("Тест 4 провален", ", куча не инициализирована.");
        return;
    }
    check_heap(heap, "Куча после инициализации");

    void* allocator = _malloc(132);
    assert(allocator);
    if (!allocator) error_msg("Тест 4 провален", ", память не выделена.");
    check_heap(heap, "Куча после выделения памяти");
    heap_term();
    end_test(heap, HEAP_SIZE, "Тест 4");
}

void test5(void) {
    void* pre_allocator = map_pages(HEAP_START, 8, MAP_FIXED);
    assert(pre_allocator);
    void* alloc_in_filled_ptr = _malloc(132);
    assert(alloc_in_filled_ptr);
    assert(pre_allocator != alloc_in_filled_ptr);
    heap_term();
}
